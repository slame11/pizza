<?php
// HTTP
define('HTTP_SERVER', 'http://pizzafamiglia.com.ua/admin/');
define('HTTP_CATALOG', 'http://pizzafamiglia.com.ua/');

// HTTPS
define('HTTPS_SERVER', 'https://pizzafamiglia.com.ua/admin/');
define('HTTPS_CATALOG', 'https://pizzafamiglia.com.ua/');

// DIR
define('DIR_APPLICATION', '/home/pizzafam/pizzafamiglia.com.ua/www/admin/');
define('DIR_SYSTEM', '/home/pizzafam/pizzafamiglia.com.ua/www/system/');
define('DIR_IMAGE', '/home/pizzafam/pizzafamiglia.com.ua/www/image/');
define('DIR_STORAGE', '/home/pizzafam/pizzafamiglia.com.ua/storage/');
define('DIR_CATALOG', '/home/pizzafam/pizzafamiglia.com.ua/www/catalog/');
define('DIR_LANGUAGE', DIR_APPLICATION . 'language/');
define('DIR_TEMPLATE', DIR_APPLICATION . 'view/template/');
define('DIR_CONFIG', DIR_SYSTEM . 'config/');
define('DIR_CACHE', DIR_STORAGE . 'cache/');
define('DIR_DOWNLOAD', DIR_STORAGE . 'download/');
define('DIR_LOGS', DIR_STORAGE . 'logs/');
define('DIR_MODIFICATION', DIR_STORAGE . 'modification/');
define('DIR_SESSION', DIR_STORAGE . 'session/');
define('DIR_UPLOAD', DIR_STORAGE . 'upload/');

// DB
define('DB_DRIVER', 'mysqli');
define('DB_HOSTNAME', 'pizzafam.mysql.tools');
define('DB_USERNAME', 'pizzafam_db');
define('DB_PASSWORD', 'c7W2WyA5');
define('DB_DATABASE', 'pizzafam_db');
define('DB_PORT', '3306');
define('DB_PREFIX', 'oc_');

// OpenCart API
define('OPENCART_SERVER', 'https://www.opencart.com/');